<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//CRUD cast

//Read
//Tampil semua data
Route::get('/cast', [CastController::class, 'index']);
//Detail cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Create
//form tambah data
Route::get('/cast/create', [CastController::class, 'create']);
//kirim data ke database atau tambah data ke database
Route::post('/cast', [CastController::class, 'store']);

//Update
//form update cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//update data ke database berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete
//delete berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);