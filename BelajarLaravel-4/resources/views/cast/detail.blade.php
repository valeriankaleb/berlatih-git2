@extends('master')

@section('judul')
Detail Cast
@endsection

@section('content')

<table class="table">
    <thead>
      <tr>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{ $cast->nama }}</td>
        <td>{{ $cast->umur }}</td>
        <td>{{ $cast->bio }}</td>
      </tr>
    </tbody>
</table>

  <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection