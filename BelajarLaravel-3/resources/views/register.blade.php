<!DOCTYPE html>
<html>
<head>
    <title>Tugas 1 Sanbercode</title>
</head>
<body>

    <b><h1>Buat Account Baru!</h1></b>

    <form action="/welcome" method="post">
        @csrf

        <b><h3>Sign Up Form</h3></b>

        <label>First name:</label><br>
        <input type="text" name="nama_depan"><br><br>

        <label>Last name:</label><br>
        <input type="text" name="nama_belakang"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="jenis_kelamin" value="male">Male<br>
        <input type="radio" name="jenis_kelamin" value="female">Female<br>
        <input type="radio" name="jenis_kelamin" value="other">Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="Nationality">
            <option value="indonesian">Indonesia</option>
            <option value="english">Singapura</option>
            <option value="english">Malaysia</option>
            <option value="english">Thailand</option>
            <option value="other">Other</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Arabic<br>
        <input type="checkbox" name="bahasa">Japanese<br><br>

        <label>Bio:</label><br><br>
        <textarea name="message" cols="29" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
    
    
</body>
</html>